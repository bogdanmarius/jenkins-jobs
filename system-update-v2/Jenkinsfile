pipeline {
    agent any

    stages {
        stage('Warming up') {
            steps {
                sh '''#!/bin/bash
                echo "Checking $HOSTNAME for load and uptime"
                uptime
                sleep 2
                '''
            }
        }
        stage('Starting system update') {
            steps {
                sh '''#!/bin/bash
                    echo "Preparing $HOSTNAME for system update pipeline"
                    sleep 2
                '''
            }
        }
        stage('Cleaning file cache') {
            steps {
                sh '''#!/bin/bash
                    sudo dnf clean all
                    sleep 2
                    echo "Cache is now clean"
                    sleep 2
                '''
            }
        }
        stage('Checking for system updates') {
            steps {
                sh '''#!/bin/bash
                    echo "Checking for updates... This might take a while"
                    sudo dnf check-update
                    sleep 2
                '''
            }
        }
        stage('Moving to install updates') {
            steps {
                script {
                    // Define Variable
                    def USER_INPUT = input(
                    message: 'Install updates now - Yes or No ?',
                    parameters: [
                            [$class: 'ChoiceParameterDefinition',
                             choices: ['No','Yes'].join('\n'),
                             name: 'input',
                             description: 'Menu - select option']
                        ])

                    echo "The answer is: ${USER_INPUT}"

                    if( "${USER_INPUT}" == "Yes"){
                        sh '''#!/bin/bash
                           echo "Moving to install discovered updates"
                           sudo dnf update -y
                           '''
                        }
                    else{
                        sh '''#!/bin/bash
                           echo "Update process aborted"
                        '''
                    }
                }
            }
        }
    }

    post {
        always {
            echo 'JOB DETAILS'
            echo "BUILD STATUS: ${currentBuild.currentResult}: ${env.JOB_NAME}"
            sh '''#!/bin/bash
            echo -e "EXECUTOR DETAILS"
            uname -a
            cat /etc/*release
            '''
        }
    }
}
