pipeline {
    agent any
    options {
        timestamps()
        ansiColor('xterm')
    }

    parameters {
        choice(name: 'command', choices: ['sudo service'], description: 'Linux commands to use. Available: service.')
        string(name: 'parameter', defaultValue: 'httpd', description: 'Parameter to use, service name to restart. Examples: httpd, mysqld, grafana-server')
        choice(name: 'payload', choices: ['status', 'start', 'stop', 'restart'], description: 'Payload to use for the service. Available: restart, start, stop, status.')
    }

    stages {
        stage('Build details') {
            steps {
                // using bash to echo a descriptive message
                sh('echo [display build details]:')
                // display message using jenkins to echo
                echo "Running BUILD ${env.BUILD_ID} on SERVER ${env.JENKINS_URL}"
            }
        }
        stage('Display command') {
            steps {
                // display message using jenkins to echo
                echo "[command input]: ${params.command}"
            }
        }
        stage('Display payload') {
            steps {
                // using bash to echo and display parameters
                sh("echo [payload inserted]: $parameter ${payload}")
            }
        }
        stage('Display command + payload') {
            steps {
                // using bash to echo a descriptive message
                sh('echo [display command and payload]:')
                // display message using jenkins to echo
                echo "${params.command} ${parameter} ${params.payload}"
            }
        }
        stage('Execute command + payload') {
            steps {
                // using bash to echo a descriptive message
                sh('echo [execute command and payload]:')
                // using bash to execute the command from the strings
                sh("${command} ${parameter} ${payload}")
            }
        }
    }
}
